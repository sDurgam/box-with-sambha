package com.example.squaregame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import sph.durga.boxwithsambha.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class Level05 extends LevelBaseActivity implements OnClickListener 
{



	ImageButton btnSq05a;
	ImageButton btnSq05b;
	ImageButton btnSq05c;
	ImageButton btnSq05d;
	ImageButton btnSq05e;

	int imgHeight05a;
	int imgWidth05a;

	int bonusCnt;

	int topRandCount;

	int[] leftArray05a;
	int[] leftArray05b;
	int[] leftArray05c;
	int[] leftArray05d;
	int[] leftArray05e;

	int left05Index = 0;
	int sBonusIndex = 0;


	ArrayList<Long> bonusTick;
	ArrayList<Integer> leftList;
	int leftArrIndex;
	sRandom sBonus;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level05);
		linLayout = (LinearLayout) findViewById(R.id.lev05linLayout);
		btnSq05a = (ImageButton) findViewById(R.id.btnSq05a);
		btnSq05b = (ImageButton) findViewById(R.id.btnSq05b);
		btnSq05c = (ImageButton) findViewById(R.id.btnSq05c);
		btnSq05d = (ImageButton) findViewById(R.id.btnSq05d);
		btnSq05e = (ImageButton) findViewById(R.id.btnSq05e);
		sqFrame = (FrameLayout) findViewById(R.id.sqArea05);
		scoreBarId = R.id.scoreBar05;
		timeBarId = R.id.timeBar05;
		restartBtn = (ImageButton) findViewById(R.id.restart05Btn);
		btnSq05a.setOnClickListener(this);
		btnSq05b.setOnClickListener(this);
		btnSq05c.setOnClickListener(this);
		btnSq05d.setOnClickListener(this);
		btnSq05e.setOnClickListener(this);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) 
	{
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus)
		{
			if(sAlertDialog == null)
			{
				maxHeight = (int) ((linLayout.getHeight())*(0.88));
				maxWidth = linLayout.getWidth();
				imgHeight05a = btnSq05a.getHeight();
				imgWidth05a = btnSq05a.getWidth();
				InitClasses();
			}
			else if(sAlertDialog.isCancelled)
			{
				StartCountDownTimer();
				StartTimer(level.getDelay1(), level.getInterval1(), 1);
				StartBonusTimer1();
			}
		}
	}

	private void InitClasses()
	{
		bonusTick = new ArrayList<Long>();
		//Need to change this for level 5 when seconds change
		sBonus = new sRandom(randSeed.nextLong(), 8000);
		randCount = Constants.level05RandCount;
		topRandCount = 5 * randCount;
		leftArray05a =  new int[randCount];
		leftArray05b =  new int[randCount];
		leftArray05c =  new int[randCount];
		leftArray05d =  new int[randCount];
		leftArray05e =  new int[randCount];
		randTop = new sRandom(randSeed.nextLong(), maxHeight - imgHeight05a);
		topArray = new int[topRandCount];
		topArray = randTop.GetArray(topRandCount);

		//get delay interval for bonus square
		bonusTick = sBonus.GetBonusTimerArray();

		leftList = new ArrayList<Integer>();

		GetRandomRegionValues();
		showRulesDialog();
	}

	class RepositionImgButtonTask extends TimerTask
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() 
			{  
				@Override
				public void run() 
				{
					if(!Constants.wait)
					{
						RepositionImgButton();
					}
				}
			});
		}
	}

	class RepositionBonusImgButtonTask extends TimerTask
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() 
			{  
				@Override
				public void run() 
				{
					//show the bonus button
					bonusCnt = 1;
					btnSq05e.setVisibility(View.VISIBLE);
					btnSq05e.setEnabled(true);
				}
			});
		}
	}

	public void StartTimer(int delay, int interval, int option)
	{
		super.StartTimer(delay, interval, new RepositionImgButtonTask(), option);
	}

	//First Bonus Icon
	private void StartBonusTimer1()
	{
		bonusCnt = 0;
		bonusTimer1 = new Timer();
		long r;
		r = bonusTick.get(0);
		bonusTimer1.schedule(new RepositionBonusImgButtonTask(), r);
	}

	//Second Bonus Icon
	private void StartBonusTimer2()
	{		
		btnSq05e.setVisibility(View.INVISIBLE);
		CancelBonusTimer1();
		bonusCnt = 0;
		if(bonusTimer2 == null)
		{
			bonusTimer2 = new Timer();
			long r;
			r = bonusTick.get(1);
			bonusTimer2.schedule(new RepositionBonusImgButtonTask(), r);
		}
	}


	public boolean RepositionImgButton()
	{
		if(getCntTimer() != null && !getCntTimer().hasStarted)
		{
			getCntTimer().start();
		}
		if(!btnSq05a.isEnabled())
		{
			btnSq05a.setEnabled(true);
			btnSq05b.setEnabled(true);
			btnSq05c.setEnabled(true);
			btnSq05d.setEnabled(true);
		}
		if(!endGame && leftArrIndex < Constants.GetMaxRandom(3))
		{
			params = (android.widget.FrameLayout.LayoutParams) btnSq05a.getLayoutParams();
			params.leftMargin = leftArray05a[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq05a.setLayoutParams(params);

			params = (android.widget.FrameLayout.LayoutParams) btnSq05b.getLayoutParams();
			params.leftMargin = leftArray05b[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq05b.setLayoutParams(params);

			params = (android.widget.FrameLayout.LayoutParams) btnSq05c.getLayoutParams();
			params.leftMargin = leftArray05c[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq05c.setLayoutParams(params);

			params = (android.widget.FrameLayout.LayoutParams) btnSq05d.getLayoutParams();
			params.leftMargin = leftArray05d[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq05d.setLayoutParams(params);

			params = (android.widget.FrameLayout.LayoutParams) btnSq05e.getLayoutParams();
			params.leftMargin = leftArray05e[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq05e.setLayoutParams(params);
			left05Index = leftArray05e[leftArrIndex];

			if(bonusCnt > 0 && bonusCnt < (Constants.minBonusTime/1000))
			{
				bonusCnt ++;
			}
			else if(bonusCnt == (Constants.minBonusTime/1000))
			{
				btnSq05e.setVisibility(View.INVISIBLE);
				StartBonusTimer2();
			}
			leftArrIndex ++;
		}
		return true;
	}

	@Override
	public void onClick(View view)
	{
		//		int imgId01 = 0;
		//		int imgId02 = 0;
		//		if(view == btnSq05a)
		//		{
		//			imgId01 = R.drawable.glad_icon;
		//			imgId02 = R.drawable.crying_icon;
		//		}
		//		else if(view == btnSq05e)
		//		{
		//			bonusCnt = 0;
		//			imgId01 = R.drawable.glad_icon;
		//			imgId02 = R.drawable.glad_icon;
		//		}
		//		else
		//		{
		//			imgId01 = R.drawable.crying_icon;
		//			imgId02 = R.drawable.glad_icon;
		//		}
		if(view == btnSq05a)
		{
			gameScore++;
		}
		else if(view == btnSq05e)
		{
			gameScore = gameScore + 2;
			btnSq05e.setVisibility(View.INVISIBLE);
			StartBonusTimer2();
		}
		else //if images 5b or 5c or 5d then gamescore -- yet to add bonus image
		{
			gameScore --;
		}
		if(gameScore == level.getSuccessLimit())
		{
			//Game finishes
			CallResultFragment();
		}
		else 
		{

			Constants.handler.sendEmptyMessage(1);
			Constants.wait = false;
		}
		btnSq05a.setEnabled(false);
		btnSq05b.setEnabled(false);
		btnSq05c.setEnabled(false);
		btnSq05d.setEnabled(false);
		btnSq05e.setEnabled(false);
		SetScoreProgress();
	}

	public void GetRandomRegionValues() 
	{
		leftList = GetRandomRegions(maxWidth, imgWidth05a);
		for(int i =0; i < randCount; i++)
		{
			Collections.shuffle(leftList);		
			leftArray05a[i] = leftList.get(0);
			leftArray05b[i] = leftList.get(1);
			leftArray05c[i] = leftList.get(2);
			leftArray05d[i] = leftList.get(3);
			leftArray05e[i] = leftList.get(4);
		}
	}

}