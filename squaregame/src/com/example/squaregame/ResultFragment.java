package com.example.squaregame;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import sph.durga.boxwithsambha.R;

public class ResultFragment extends Fragment {

	Button nextBtn;
	//Button quitBtn;
	TextView resultTxt;

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.resultfragment, container, false);
		Bundle args = getArguments();
		if(args != null)
		{
			nextBtn = (Button) view.findViewById(R.id.nextButton);
			//quitBtn = (Button) view.findViewById(R.id.quitBtn);
			resultTxt = (TextView) view.findViewById(R.id.resultText);
			if(args.get(Constants.nextActionMsg) != null)
				nextBtn.setText(args.get(Constants.nextActionMsg).toString());
			if(args.get(Constants.resultMsg) != null)
				resultTxt.setText(args.get(Constants.resultMsg).toString());
		}
		return view;

	}


	/* (non-Javadoc)
	 * @see android.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreateView(java.lang.String, android.content.Context, android.util.AttributeSet)
	 */

	/* (non-Javadoc)
	 * @see android.app.Fragment#onPause()
	 */
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	/* (non-Javadoc)
	 * @see android.app.Fragment#onStop()
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}


}
