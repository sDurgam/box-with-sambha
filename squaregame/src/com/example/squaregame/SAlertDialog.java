package com.example.squaregame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import sph.durga.boxwithsambha.R;


public class SAlertDialog
{
	Context mContext;
	boolean isCancelled;
	public SAlertDialog(Context ctx, int currentLevel, String ruleMsg) {
		mContext = ctx;
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
		// set title
		alertDialogBuilder.setTitle(String.valueOf("ROUND " + currentLevel));
		alertDialogBuilder.setCancelable(false);
		
		 if(currentLevel == 3)
		 {
			 LayoutInflater inflater = ((LevelBaseActivity) ctx).getLayoutInflater();
			 alertDialogBuilder.setView(inflater.inflate(R.layout.customdialog03, null));
			 alertDialogBuilder
				.setNegativeButton("START",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) 
					{
					//start timers
						dialog.cancel();
						isCancelled = true;
					}
				  });
			 
		 }
		 else if(currentLevel == 5)
		 {
			 LayoutInflater inflater = ((LevelBaseActivity) ctx).getLayoutInflater();
			 alertDialogBuilder.setView(inflater.inflate(R.layout.customdialog05, null));
			 alertDialogBuilder
				.setNegativeButton("START",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) 
					{
					//start timers
						dialog.cancel();
						isCancelled = true;
					}
				  });
			 
		 }
		 else
		 {
			
			// set dialog message
			alertDialogBuilder
				.setMessage(ruleMsg)
				.setNegativeButton("START",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) 
					{
					//start timers
						dialog.cancel();
						isCancelled = true;
					}
				  });
//				.setNegativeButton("No",new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int id) {
//						// if this button is clicked, just close
//						// the dialog box and do nothing
//						dialog.cancel();
//					}
//				});
		 }

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			isCancelled = false;
			// show it
			alertDialog.show();
			
	}
}