package com.example.squaregame;

import java.util.TimerTask;
import sph.durga.boxwithsambha.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;


public class Level03 extends LevelBaseActivity implements OnClickListener
{
	ImageButton btnSq;

	int imgHeight;
	int imgWidth;

	int[] leftArray;

	int[] imgRandArray;
	int[] bitArray = {0, 1};


	sRandom randLeft;
	sRandom randImg;

	int leftArrIndex = 0;
	int randArrIndex = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level03);
		linLayout = (LinearLayout) findViewById(R.id.lev03linLayout);
		restartBtn = (ImageButton) findViewById(R.id.restart03Btn);
		sqFrame = (FrameLayout) findViewById(R.id.sqArea03);
		btnSq = (ImageButton) findViewById(R.id.btnSq03);
		btnSq.setTag(true);
		scoreBarId = R.id.scoreBar03;
		timeBarId = R.id.timeBar03;
		btnSq.setOnClickListener(this);	
	}

	class RepositionImgButtonTask extends TimerTask
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() 
			{  
				@Override
				public void run() 
				{
					if(!Constants.wait)
					{
						RepositionImgButton();
					}
				}
			});
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus)
		{
			if(sAlertDialog == null)
			{
				maxHeight = (int) ((linLayout.getHeight())*(0.88));
				maxWidth = linLayout.getWidth();
				imgHeight = btnSq.getHeight();
				imgWidth = btnSq.getWidth();
				InitClasses();
			}
			else if(sAlertDialog.isCancelled)
			{
				StartCountDownTimer();
				StartTimer(level.getDelay1(), level.getInterval1(), 1);
			}
		}
	}



	private void InitClasses()
	{
		randLeft = new sRandom(randSeed.nextLong(), maxWidth - imgWidth);
		randTop = new sRandom(randSeed.nextLong(), maxHeight - imgHeight);
		randImg = new sRandom(randSeed.nextLong(), Constants.totalImgRange);

		leftArray = randLeft.GetArray(Constants.level03RandCount);
		topArray = randTop.GetArray(Constants.level03RandCount);
		imgRandArray = randImg.GetArray(Constants.level03RandCount);

		int currentLevel = 3;
		//		level = Constants.GetCurrentLevelObject(currentLevel);
		//		tickduration = level.getTickduration();

		Constants.UpdateLevel3SuccessScore(currentLevel, imgRandArray);

		showRulesDialog();
	}


	public void StartTimer(int delay, int interval, int option)
	{
		super.StartTimer(delay, interval, new RepositionImgButtonTask(), option);
	}



	@Override
	public void onClick(View view)
	{
		if(view == btnSq)
		{
			//NEED TO UNCOMMENT FOR ACTUAL IMAGES
			//	btnSq.setImageResource(R.drawable.glad_icon);
			btnSq.setEnabled(false);
			if((Boolean)btnSq.getTag() == true)
			{
				gameScore ++;
			}
			else
			{
				gameScore --;
			}
			if(gameScore == level.getSuccessLimit())
			{
				//Game finishes
				CallResultFragment();
			}
			else if(gameScore < level.getSuccessLimit())
			{
				Constants.handler.sendEmptyMessage(1);
				Constants.wait = false;
			}
			SetScoreProgress();
		}
	}

	protected boolean RepositionImgButton()
	{
		if(getCntTimer() != null && !getCntTimer().hasStarted)
		{
			getCntTimer().start();
		}
		if(!btnSq.isEnabled())
		{
			btnSq.setEnabled(true);
			//NEED TO UNCOMMENT FOR ACTUAL IMAGES
			//btnSq.setImageResource(R.drawable.crying_icon);
		}
		if(leftArrIndex < Constants.level03RandCount)
		{
			params = (android.widget.FrameLayout.LayoutParams) btnSq.getLayoutParams();
			params.leftMargin = leftArray[leftArrIndex ++];
			params.topMargin = topArray[topArrIndex ++];
			btnSq.setLayoutParams(params);
			if(imgRandArray[randArrIndex] <= Constants.cryIconRange)
			{
				btnSq.setImageResource(R.drawable.sambha1);
				btnSq.setTag(true);
			}
			else
			{
				btnSq.setImageResource(R.drawable.friend);
				btnSq.setTag(false);
			}
			randArrIndex++;
		}
		return true;
	}
}
