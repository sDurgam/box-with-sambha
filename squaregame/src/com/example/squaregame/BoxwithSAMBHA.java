package com.example.squaregame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import sph.durga.boxwithsambha.R;

public class BoxwithSAMBHA extends Activity
  implements View.OnClickListener
{
  Button startBtn;
  TextView rule02Btn;
  String rule02txt;

  public void onClick(View paramView)
  {
    if (paramView == this.startBtn)
    {
      startActivity(new Intent(this, MainActivity.class));
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.boxwithsambha);
    DisplayMetrics dpMetrics = getApplicationContext().getResources().getDisplayMetrics();
    int densityDpi =   dpMetrics.densityDpi;
    int screenHeight = dpMetrics.heightPixels;
    int screenWidth = dpMetrics.widthPixels;
    int imgSize = 48;
    switch(densityDpi)
    {
    case  DisplayMetrics.DENSITY_LOW: 
    	imgSize = 54;
    	break;
    case DisplayMetrics.DENSITY_MEDIUM:
    	imgSize = 72;
    	break;
    case DisplayMetrics.DENSITY_TV:
    	imgSize = 96;
    	break;
    case DisplayMetrics.DENSITY_HIGH:
    	imgSize = 108;
    	break;
    case DisplayMetrics.DENSITY_XHIGH:
    	imgSize = 144;
    	break;
    case DisplayMetrics.DENSITY_XXHIGH:
    	imgSize = 216;
    	break;
    case DisplayMetrics.DENSITY_XXXHIGH:
    	imgSize = 288;
    	break;
    }
	Constants.FittsLaw(screenHeight, screenWidth, imgSize);
	//Constants.UpdateLevelInterval(area, screenSize);
    rule02txt = "Score " + Constants.successLimit1 + " in 60 seconds to win";
    this.startBtn = ((Button)findViewById(R.id.startBtn));
    this.startBtn.setOnClickListener(this);
    Constants.SetLevelParameters();
    
  }
  /* (non-Javadoc)
 * @see android.app.Activity#onPause()
 */
@Override
protected void onPause() {
	// TODO Auto-generated method stub

	super.onPause();
}

/* (non-Javadoc)
 * @see android.app.Activity#onResume()
 */
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
	SharedPreferences.Editor editor = sharedPref.edit();
	//save level
	editor.putInt(getString(R.string.saved_level), 0);
	//save timestamp
	editor.putLong(getString(R.string.current_timestamp), System.currentTimeMillis());
	editor.commit();
}

@Override
	public void onBackPressed()
	{
	  Intent startMain = new Intent(Intent.ACTION_MAIN);
	  startMain.addCategory(Intent.CATEGORY_HOME);
	  startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	  startActivity(startMain);
	}
}