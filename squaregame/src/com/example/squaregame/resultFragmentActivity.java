package com.example.squaregame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import sph.durga.boxwithsambha.R;

public class resultFragmentActivity extends FragmentActivity implements OnClickListener 
{
	Button nextActionBtn;
	TextView resultMsgTxt;
	String resultMsg;
	String nextAction;
	Integer currentLevel;
	Round level;

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		//save level
		editor.putInt(getString(R.string.saved_level), currentLevel);
		//save timestamp
		editor.putLong(getString(R.string.current_timestamp), System.currentTimeMillis());
		editor.commit();
		super.onPause();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//do nothing
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
		long curTS = System.currentTimeMillis();
		long timeStamp = sharedPref.getLong(getString(R.string.current_timestamp), curTS);
		if(curTS - timeStamp > 120000)
		{
			//display home
			Intent in = new Intent(this, BoxwithSAMBHA.class);
			startActivity(in);
		}
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.resultfragment);
		resultMsgTxt = (TextView) findViewById(R.id.resultText);
		nextActionBtn = (Button) findViewById(R.id.nextButton);

		Bundle b = this.getIntent().getExtras();
		resultMsg = b.getString(Constants.resultMsg);
		nextAction = b.getString(Constants.nextActionMsg);
		currentLevel = b.getInt(Constants.currentRound);
		resultMsgTxt.setText(resultMsg);
		nextActionBtn.setText(nextAction);
		if(!resultMsg.equals(Constants.loseMsg))
		{
			currentLevel ++;
		}
		if(resultMsg.equals(Constants.youWinAllLevels))
		{
			//set text size of the result display to 22sp
			resultMsgTxt.setTextSize(22);
		}
		level = Constants.GetCurrentLevelObject(currentLevel);
		nextActionBtn.setOnClickListener(this);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus)
		{
			nextActionBtn.setEnabled(true);
		}
	}


	@Override
	public void onClick(View view) 
	{
		// TODO Auto-generated method stub
		if(view == nextActionBtn)
		{
//			//All Levels won
//			if(nextAction == Constants.HOME)
//			{
//				currentLevel = 0;
//			}
			CallLevelActivity();
		}
	}

	private void CallLevelActivity()
	{
		Intent in = null;
		//All Levels won
		if(currentLevel == 6)
		{
			in = new Intent(this, BoxwithSAMBHA.class);
		}
		else if(currentLevel == 1 || currentLevel == 2)
		{
			in = new Intent(this, MainActivity.class);
		}
		else if(currentLevel == 3)
		{
			in = new Intent(this, Level03.class);
		}
		else if(currentLevel == 4)
		{
			in = new Intent(this, Level04.class);
		}
		else if(currentLevel == 5)
		{
			in = new Intent(this, Level05.class);
		}
		SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putInt(getString(R.string.saved_level), currentLevel);
		editor.commit();
		startActivity(in);
	}
}
