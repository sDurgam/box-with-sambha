package com.example.squaregame;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import sph.durga.boxwithsambha.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


public class LevelBaseActivity extends Activity {


	protected Timer timer;
	Timer bonusTimer1;
	Timer bonusTimer2;
	private MyCountDownTimer cntTimer;
	long tickduration;

	int gameScore;
	int currentLevel;
	boolean endGame;
	Round level;

	ImageButton btnSq;
	ImageButton restartBtn;

	//progress bar for game score
	ProgressBar scoreBar;
	int scoreBarId;

	//progress bar for time
	ProgressBar timeBar;
	int timeBarId;

	//for alert dialog
	SAlertDialog sAlertDialog = null;

	LinearLayout linLayout;
	FrameLayout sqFrame;
	FrameLayout.LayoutParams params;
	int maxHeight;
	int maxWidth;

	int randCount;
	sRandom randTop;
	int[] topArray;
	int topArrIndex = 0;

	Random randSeed = new Random(System.currentTimeMillis());
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	//initialize timer and score progress bars
	protected void IntializeBaseValues()
	{
		scoreBar = (ProgressBar) findViewById(scoreBarId);
		timeBar = (ProgressBar) findViewById(timeBarId);
		//set gameScore to 0 in the beginning
		scoreBar.setProgress(0);
		scoreBar.setMax(level.getSuccessLimit());
		//set time to maximum value in the beginning
		int time = (int)level.getTickduration(); 
		timeBar.setMax(time);
		timeBar.setProgress(time);
		Constants.wait = false;
		restartBtn.setOnClickListener(listener);
	}
	OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v)
		{
			// TODO Auto-generated method stub
			CancelCntTimer();
			CancelTimer();
			int currentLevel = level.getNum();
			Intent in01 = null;
			if(currentLevel == 1 || currentLevel == 2)
			{
				in01 = new Intent(getApplicationContext(), MainActivity.class);
			}
			else if(currentLevel == 3)
			{
				in01 = new Intent(getApplicationContext(), Level03.class);
			}
			else if(currentLevel == 4)
			{
				in01 = new Intent(getApplicationContext(), Level04.class);
			}
			else if(currentLevel == 5)
			{
				CancelBonusTimer1();
				CancelBonusTimer2();
				in01 = new Intent(getApplicationContext(), Level05.class);
			}
			startActivity(in01);
		}
	};

	protected void SetScoreProgress()
	{
		if(gameScore < 0) //once it turns negative change slider color
		{
			if(gameScore == -1)
			{
				scoreBar.setProgressDrawable(getResources().getDrawable(R.drawable.redprogress));
			}
			scoreBar.setProgress(-gameScore);
		}
		else
		{
			if(gameScore == 0)
			{
				scoreBar.setProgressDrawable(getResources().getDrawable(R.drawable.scoreprogress));
			}
			scoreBar.setProgress(gameScore);
		}
	}



	/**
	 * @return the cntTimer
	 */
	public MyCountDownTimer getCntTimer() {
		return cntTimer;
	}

	/**
	 * @param cntTimer the cntTimer to set
	 */
	public void setCntTimer(MyCountDownTimer cntTimer) {
		this.cntTimer = cntTimer;
	}

	//show the rules of the level in a alert box and when the clicks ok only then your timer starts
	protected void showRulesDialog()
	{
		String ruleMsg = "";
		//initialize time and score bars
		IntializeBaseValues();
		if(currentLevel == 1)
		{
			//ruleMsg = getResources().getString(R.string.instructions1a) + level.getSuccessLimit() + getResources().getString(R.string.scoreInstructionb);
			ruleMsg = getResources().getString(R.string.instructions1a);
		}
		else if(currentLevel  == 2)
		{
			ruleMsg = getResources().getString(R.string.instructions2a);
		}
		else if(currentLevel  == 3)
		{
			//	ruleMsg = getResources().getString(R.string.instructions3a);
		}
		else if(currentLevel == 4)
		{
			ruleMsg = getResources().getString(R.string.instrcutions4a);
		}
		else if(currentLevel == 5)
		{
			//	ruleMsg = getResources().getString(R.string.instructions5a);
		}
		sAlertDialog = new SAlertDialog(this, currentLevel, ruleMsg);
	}



	/* (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//kill timers
		CancelTimer();
		CancelCntTimer();
		//kill alertdialog
		sAlertDialog = null;
		SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		//save level
		editor.putInt(getString(R.string.saved_level), currentLevel);
		//save timestamp
		editor.putLong(getString(R.string.current_timestamp), System.currentTimeMillis());
		editor.commit();

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onRestart()
	 */
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() 
	{
		// TODO Auto-generated method stub
		long curStamp = System.currentTimeMillis();
		SharedPreferences sharedPref = this.getSharedPreferences(Constants.PRIVATEPREF, Context.MODE_PRIVATE);
		currentLevel = sharedPref.getInt(getResources().getString(R.string.saved_level), 1);
		long timeStamp = sharedPref.getLong(getString(R.string.current_timestamp), curStamp);
		if(curStamp - timeStamp > 120000)
		{
			//display home
			Intent in = new Intent(this, BoxwithSAMBHA.class);
			startActivity(in);
		}
		else
		{
			if(currentLevel == 0)
				currentLevel = 1;
			level = Constants.GetCurrentLevelObject(currentLevel);
			gameScore = 0;
			tickduration = level.getTickduration();
			super.onResume();
		}
	}

	@Override
	public void onBackPressed()
	{
		CancelTimer();
		CancelCntTimer();
		//		Intent in = null;
		//		if(currentLevel == 1)
		//		{
		//			in = new Intent(this, BoxwithSAMBHA.class);
		//		}
		//		else if(currentLevel == 2 || currentLevel == 3)
		//		{
		//			in = new Intent(this, MainActivity.class);
		//			//	in.putExtra(Constants.currentLevel, currentLevel - 1);
		//		}
		//		else if(currentLevel == 4)
		//		{
		//			in = new Intent(this, Level03.class);
		//			//	in.putExtra(Constants.currentLevel, currentLevel - 1);
		//		}
		//		else if(currentLevel == 5)
		//		{
		//			in = new Intent(this, Level04.class);
		//			//	in.putExtra(Constants.currentLevel, level.getNum() - 1);
		//		}
		//		currentLevel = currentLevel - 1;
		//		startActivity(in);
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	protected void onDestroy() 
	{
		CancelTimer();
		CancelCntTimer();

		super.onDestroy();
	}

	public void StartTimer(int delay, int interval, TimerTask timerTask, int option)
	{
		if(option == 2)
		{
			//new tick duration = time from text box
			StartCountDownTimer();
		}
		timer = new Timer();
		timer.scheduleAtFixedRate(timerTask, delay, interval);
	}


	public void StartCountDownTimer()
	{
		if(cntTimer == null)
		{
			cntTimer = new MyCountDownTimer(tickduration, level.getCountDownInterval(), currentLevel, level.getSuccessLimit(), timeBar, this);

		}
	}


	//Cancel bonus timer 01 for Level 05
	protected void CancelBonusTimer1()
	{
		if(bonusTimer1 != null)
		{
			bonusTimer1.cancel();
			bonusTimer1.purge();
			bonusTimer1 = null;
		}
	}


	//Cancel bonus timer 02 for Level 05
	protected void CancelBonusTimer2()
	{
		if(bonusTimer2 != null)
		{
			bonusTimer2.cancel();
			bonusTimer2.purge();
			bonusTimer2 = null;
		}
	}

	//cancel countdown timer
	protected void CancelCntTimer()
	{
		if(cntTimer != null)
		{	
			tickduration = cntTimer.timeLeft * 1000;
			cntTimer.cancel();
			cntTimer = null;
		}
	}

	//Cancel timer
	protected void CancelTimer()
	{
		if(timer != null)
		{
			timer.cancel();
			timer.purge();
			timer = null;
		}
	}

	public void CallResultFragment()
	{
		//yet to add for level 05
		//				((Level05)ctx).CancelBonusTimer1();
		CancelCntTimer();
		CancelTimer();
		String nextAction = null;
		String resultMsg = null;
		if(gameScore == level.getSuccessLimit())
		{
			if(currentLevel == 5)
			{
				nextAction = Constants.HOME;
				resultMsg = Constants.youWinAllLevels;
			}
			else
			{
				nextAction = Constants.nextLevelMsg;
				resultMsg = Constants.winMsg;
			}
		}
		else if(gameScore < level.getSuccessLimit())
		{
			nextAction = Constants.restartMsg;
			resultMsg = Constants.loseMsg;
		}
		Intent in = new Intent(this, resultFragmentActivity.class);
		in.putExtra(Constants.nextActionMsg, nextAction);
		in.putExtra(Constants.resultMsg, resultMsg);
		in.putExtra(Constants.currentRound, currentLevel);
		in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(in);		
	}

	//Gets random regions for the level 05 based on the maximum width of the screen and height of the image
	protected ArrayList<Integer> GetRandomRegions(int maxWidth, int imgWidth)
	{
		ArrayList<Integer> regionsList = new ArrayList<Integer>();
		int temp;
		int numRegions = maxWidth/imgWidth;
		if(maxWidth % imgWidth == 0)
		{
			temp = maxWidth/numRegions;
		}
		else
		{
			temp = maxWidth/(numRegions + 1);
		}
		for(int i =0; i < numRegions; i++)
		{ 
			regionsList.add(i * temp);
		}
		return regionsList;
	}

}
