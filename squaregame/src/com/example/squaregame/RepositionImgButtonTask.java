package com.example.squaregame;

import java.util.TimerTask;
import sph.durga.boxwithsambha.R;

import android.os.CountDownTimer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

class RepositionImgButtonTask extends TimerTask
{
	CountDownTimer cntTimer;
	ImageButton btnSq;

	int[] leftArray;
	int[] topArray;
	int[] imgRandArray;
	int[] bitArray = {0, 1};

	sRandom randLeft;
	sRandom randTop;
	sRandom randImg;

	int leftArrIndex;
	int topArrIndex;
	int randArrIndex;



	LinearLayout relLayout;
	FrameLayout sqFrame;
	FrameLayout.LayoutParams params;

	TextView scoreTxt;
	TextView timeLeftTxt;

	long timeLeft;
	long animDuration;
	long tickduration;
	long countDownInterval;
	int successLimit;


	@Override
	public void run() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() 
		{  
			@Override
			public void run() 
			{
				if(!Constants.wait)
				{
					RepositionImgButton();
				}
			}
		}).start();
	}


	private boolean RepositionImgButton()
	{
		if(cntTimer != null && !((MyCountDownTimer)cntTimer).hasStarted)
		{
			cntTimer.start();
		}
		if(!btnSq.isEnabled())
		{
			btnSq.setEnabled(true);
		}
		if(leftArrIndex < Constants.level03RandCount)
		{
			params = (android.widget.FrameLayout.LayoutParams) btnSq.getLayoutParams();
			params.leftMargin = leftArray[leftArrIndex ++];
			params.topMargin = topArray[topArrIndex ++];
			btnSq.setLayoutParams(params);
			if(imgRandArray[randArrIndex] == 0)
			{
				btnSq.setImageResource(R.drawable.sambha1);
				btnSq.setTag(true);
			}
			else
			{
				btnSq.setImageResource(R.drawable.friend);
				btnSq.setTag(false);
			}
			randArrIndex++;
		}
		return true;
	}



}