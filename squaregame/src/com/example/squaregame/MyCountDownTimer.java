package com.example.squaregame;
import java.util.Timer;
import android.content.Context;
import android.os.CountDownTimer;
import android.widget.ProgressBar;

public class MyCountDownTimer extends CountDownTimer
{
	long timeLeft;
	boolean endGame;
	int gameScore = 0;
	int successLimit;
	int currentLevel;
	boolean hasStarted;

	ProgressBar timeBar;
	Context ctx;
	Timer timer;
	Timer bonusTimer;

	public MyCountDownTimer(long millisInFuture, long countDownInterval, int currentLevel, int successLimit, ProgressBar timeBar, Context ctx) {
		super(millisInFuture, countDownInterval);
		this.timeBar = timeBar;
		this.ctx = ctx;
		this.currentLevel = currentLevel;
		this.successLimit = successLimit;
	}
	/**
	 * @return the timeLeft
	 */
	public long getTimeLeft() {
		return timeLeft;
	}
	/**
	 * @param timeLeft the timeLeft to set
	 */
	public void setTimeLeft(long timeLeft) {
		this.timeLeft = timeLeft;
	}

	/**
	 * @return the gameScore
	 */
	public int getGameScore() {
		return gameScore;
	}
	/**
	 * @param gameScore the gameScore to set
	 */
	public void setGameScore(int gameScore) {
		this.gameScore = gameScore;
	}
	public void onTick(long millisUntilFinished) 
	{
		if(!hasStarted)
			hasStarted = true;
		//timeLeft = millisUntilFinished/1000;
		timeBar.setProgress((int) millisUntilFinished);
	}
	
	public void CancelCntTimer(long tickduration)
	{
		//timeLeftTxt.setText(String.valueOf(tickduration/1000));
		this.cancel();
	}
	
	public void onFinish() 
	{
		((LevelBaseActivity)ctx).CallResultFragment();
	}
}

