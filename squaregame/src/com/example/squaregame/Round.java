package com.example.squaregame;

class Round {

	private long timeLeft;
	private long tickduration;
	private long countDownInterval;
	private long successScore;
	private int delay;
	private int interval;
	private int successLimit;
	private int num;
	/**
	 * @return the num
	 */
	public int getNum() {
		return num;
	}
	/**
	 * @param num the num to set
	 */
	public void setNum(int num) {
		this.num = num;
	}
	/**
	 * @return the delay1
	 */
	public int getDelay1() {
		return delay;
	}
	/**
	 * @param delay1 the delay1 to set
	 */
	public void setDelay1(int delay1) {
		this.delay = delay1;
	}
	/**
	 * @return the interval1
	 */
	public int getInterval1() {
		return interval;
	}
	/**
	 * @param interval1 the interval1 to set
	 */
	public void setInterval1(int interval1) {
		this.interval = interval1;
	}
	/**
	 * @return the delay2
	 */
	public int getDelay2() {
		return delay2;
	}
	/**
	 * @param delay2 the delay2 to set
	 */
	public void setDelay2(int delay2) {
		this.delay2 = delay2;
	}
	/**
	 * @return the interval2
	 */
	public int getInterval2() {
		return interval2;
	}
	/**
	 * @param interval2 the interval2 to set
	 */
	public void setInterval2(int interval2) {
		this.interval2 = interval2;
	}
	int delay2;
	int interval2;
		
	
	/**
	 * @return the successLimit
	 */
	public int getSuccessLimit() {
		return successLimit;
	}
	/**
	 * @param successLimit the successLimit to set
	 */
	public void setSuccessLimit(int successLimit) {
		this.successLimit = successLimit;
	}
	/**
	 * @return the successScore
	 */
	public long getSuccessScore() {
		return successScore;
	}
	/**
	 * @param successScore the successScore to set
	 */
	public void setSuccessScore(long successScore) {
		this.successScore = successScore;
	}
	/**
	 * @return the timeLeft
	 */
	public long getTimeLeft() {
		return timeLeft;
	}
	/**
	 * @param timeLeft the timeLeft to set
	 */
	public void setTimeLeft(long timeLeft) {
		this.timeLeft = timeLeft;
	}
	/**
	 * @return the animDuration
	 */

	/**
	 * @return the tickduration
	 */
	public long getTickduration() {
		return tickduration;
	}
	/**
	 * @param tickduration the tickduration to set
	 */
	public void setTickduration(long tickduration) {
		this.tickduration = tickduration;
	}
	/**
	 * @return the countDownInterval
	 */
	public long getCountDownInterval() {
		return countDownInterval;
	}
	/**
	 * @param countDownInterval the countDownInterval to set
	 */
	public void setCountDownInterval(long countDownInterval) {
		this.countDownInterval = countDownInterval;
	}

}
