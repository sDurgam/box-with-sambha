package com.example.squaregame;

import java.util.ArrayList;
import java.util.Random;

public class sRandom {

	Random randSeed;
	Random randomGen;
	int max;
	public sRandom(long seed, int max) {
		super();
		this.randomGen = new Random(seed);
		this.max = max;
	}

	protected int GetRandomNum()
	{
		return randomGen.nextInt(max);
	}

	protected int GetRandomNum(int maximum)
	{
		return randomGen.nextInt(maximum);
	}


	protected int[] GetArray(int count)
	{
		int[] randArray = new int[count];
		for(int i =0; i < count; i ++)
		{
			randArray[i] = GetRandomNum();
		}
		return randArray;
	}

	protected ArrayList<Long> GetBonusTimerArray()
	{
		long num1;
		long num2;
		ArrayList<Long> arrList = new ArrayList<Long>();
		num1 =	GetRandomNum();
		arrList.add(num1);
		num2 = GetRandomNum() + (Constants.bonusHalfTime - num1);
		arrList.add(num2);
		return arrList;
	}
}
