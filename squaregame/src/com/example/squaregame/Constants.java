package com.example.squaregame;

import java.util.Hashtable;
import android.os.Handler;
import android.os.Message;


public class Constants 
{
	public static Hashtable<Integer, Round> roundTable;
	static boolean wait;
	static String nextActionMsg = "Next Action";
	static String resultMsg = "resultMsg";

	static String winMsg = "YOU WIN";
	static String loseMsg = "SAMBHA WINS";

	static String nextLevelMsg = "NEXT ROUND";
	static String restartMsg = "RESTART";
	static String quitMsg = "Quit";
	static String homeMsg = "HOME";
	static String youWinAllLevels = "CONGRATULATIONS!!! for beating SAMBHA. SAMBHA will be back soon.";
	static String HOME = "HOME";

	static String resultFragment = "resultFragment";
	static String currentRound = "currentRound";
	static int MAX = 150;

	static long timeLeft1 = 0;
	static long tickduration1 = 30000;
	static long countDownInterval1 = 1000;
	static int delay1 = 0;
	//static int interval1 = 1000; - normal screen
	static int interval1 = 1000; //Large Screen
	static int successLimit1 = 10;
	static double constant_area = 501.05;
	//= 909312;
	static long timeLeft2 = 0;
	static long tickduration2 = 30000;
	static long countDownInterval2 = 1000;
	static int delay2 = 0;
	//840 - normal screen 
	//990 - large screen
	static int interval2 = 840;
	static int delay21 = 0;
	//840 - normal screen 
	//940 - large screen
	static int interval21 = 840;
	//static int interval4 = 925;
	static int successLimit2 = 6;
	static int successLimit5 = 3;
	static int level03RandCount = 300;
	static int level05RandCount = 300;
	//NEED TO CHANGE
	static int level3cnt;
	static int level5cnt;
	
	static int level5ImgCnt = 4;
	static String glad="glad";
	static String cry = "cry";
	
	static int bonusHalfTime = 15000;
	static int minBonusTime = 15000;

	//level 04
	static int totalImgRange = 1000;
	static int cryIconRange = 750;
	static String PRIVATEPREF = "privatepref";

	

	public static void SetLevelParameters()
	{
		roundTable = new Hashtable<Integer, Round>();
		//level 1 - Need to automate
		Round round1 = new Round();
		round1.setNum(1);
		round1.setTickduration(tickduration1);
		round1.setCountDownInterval(countDownInterval1);
		//update winning score
		successLimit1 = (int) ((0.7) * ((tickduration1 - 2000)/interval1));
		//successLimit1  = 1;
		round1.setSuccessLimit(successLimit1);
		round1.setInterval2(interval2);
		round1.setDelay1(delay1);
		round1.setInterval1(interval1);
		round1.setDelay2(delay1);
		round1.setInterval2(interval1);

		roundTable.put(1, round1);

		//level 2 - Need to automate
		Round round2 = new Round();
		round2.setNum(2);
		round2.setTickduration(tickduration2);
		round2.setCountDownInterval(countDownInterval2);
		successLimit2 = (int) ((0.8) * ((tickduration2 - 1000)/interval2));
		round2.setSuccessLimit(successLimit2);
		//round2.setSuccessLimit(2);
		round2.setDelay1(delay2);
		round2.setInterval1(interval2);
		round2.setDelay2(delay21);
		round2.setInterval2(interval21);
		roundTable.put(2, round2);
		
		//Level 3 - Need to automate
		Round round3 = new Round();
		round3.setNum(3);
		round3.setTickduration(tickduration1);
		round3.setCountDownInterval(countDownInterval1);
		successLimit2 = (int) ((0.8) * ((tickduration2 - 1000)/interval2));
		round3.setSuccessLimit(successLimit2);
		//round3.setSuccessLimit(2);
		round3.setDelay1(delay2);
		round3.setInterval1(interval2);
		round3.setDelay2(delay21);
		round3.setInterval2(interval21);
		roundTable.put(3, round3);
		
		//Level 4 - Need to automate
		Round round4 = new Round();
		round4.setNum(4);
		round4.setTickduration(tickduration2);
		round4.setCountDownInterval(countDownInterval2);
		successLimit2 = (int) ((0.8) * ((tickduration2 - 1000)/interval2));
		round4.setSuccessLimit(successLimit2);
		//round4.setSuccessLimit(2);
		round4.setInterval1(interval2);
		round4.setDelay2(delay21);
		round4.setInterval2(interval21);
		//round4.setInterval2(925);
		roundTable.put(4, round4);
		
		//Level 5 - Need to automate
		Round round5 = new Round();
		round5.setNum(5);
		round5.setTickduration(tickduration2);
		round5.setCountDownInterval(countDownInterval2);
		successLimit2 = (int) ((0.9) * ((tickduration2 - 1000)/interval2));
		round5.setSuccessLimit(successLimit2);
		//round5.setSuccessLimit(2);
		round5.setDelay1(delay2);
		round5.setInterval1(interval2);
		round5.setDelay2(delay21);
		round5.setInterval2(interval21);
		roundTable.put(5, round5);
	}
	
	public static double FittsLaw(int dimHeight, int dimWidth, int imgSize)
	{
		double validHeight = (dimHeight*0.88)-imgSize;
		double validWidth  = dimWidth - imgSize;
		double diag = Math.sqrt(validHeight*validHeight + validWidth*validWidth);
		int time = (int) Math.ceil(230+166*((Math.log(diag/imgSize+1))/(Math.log(2))));
		interval1 = time + 100;
		interval2 = interval21 = time;
		//interval4 = time + 25;
		level3cnt = (int) (Math.ceil(tickduration2/interval2)) + 1;
		level5cnt = (int) (Math.ceil(tickduration2/interval2)) + 1;
		return time;
	}

	public static Round GetCurrentLevelObject(int curLevel)
	{
		if(roundTable.containsKey(curLevel))
		{
			return Constants.roundTable.get(curLevel);
		}
		return null;
	}

	protected static final Handler handler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			if(msg.what == 10)
			{
				wait = true;
			}
		}
	};

	public static int GetMaxRandom(int currentLevel) {
		// TODO Auto-generated method stub
		if(currentLevel == 3)
		{
			return 200;
		}
		else if(currentLevel == 4)
		{
			return 150;
		}
		return 0;
	}

	public static int UpdateLevel3SuccessScore(int levelNum, int[] imgRandArray)
	{
		int count = 0;
		//level3cnt = 20000/925 + 1;
		// TODO Auto-generated method stub
		for(int i =0; i < level3cnt; i++)
		{
			if(imgRandArray[i] <= cryIconRange)
			{
				count ++;
			}
		}
		Round level = Constants.roundTable.get(levelNum);
		level.setSuccessLimit((int) (count * 0.85));
		roundTable.put(levelNum, level);
		return count;
	}


}
