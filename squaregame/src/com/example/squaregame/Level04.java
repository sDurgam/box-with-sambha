package com.example.squaregame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TimerTask;
import sph.durga.boxwithsambha.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class Level04 extends LevelBaseActivity implements OnClickListener 
{
	ImageButton btnSq04a;
	ImageButton btnSq04b;
	boolean isbtnSq01; // if cry icon clicked set it to true and use it in reposition button to set back the image

	int imgHeight1;
	int imgWidth1;
	int imgHeight2;
	int imgWidth2;

	int[] leftArray1;
	int[] leftArray2;

	ArrayList<Integer> leftList;
	int leftArrIndex;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		int currentLevel = 4;
		gameScore = 0;
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.level04);
		linLayout = (LinearLayout) findViewById(R.id.lev04linLayout);
		restartBtn = (ImageButton) findViewById(R.id.restart04Btn);
		btnSq04a = (ImageButton) findViewById(R.id.btnSq04a);
		btnSq04b = (ImageButton) findViewById(R.id.btnSq04b);

		scoreBarId = R.id.scoreBar04;
		timeBarId = R.id.timeBar04;
		level = Constants.GetCurrentLevelObject(currentLevel);
		btnSq04a.setOnClickListener(this);
		btnSq04b.setOnClickListener(this);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) 
	{
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus)
		{
			if(sAlertDialog == null)
			{
				maxHeight = (int) ((linLayout.getHeight())*(0.88));
				maxWidth = linLayout.getWidth();
				imgHeight1 = btnSq04a.getHeight();
				imgWidth1 = btnSq04a.getWidth();
				imgHeight2 = btnSq04b.getHeight();
				imgWidth2 = btnSq04b.getWidth();
				InitClasses();
			}
			else if(sAlertDialog.isCancelled)
			{
				StartCountDownTimer();
				StartTimer(level.getDelay1(),level.getInterval1(),1);
			}

		}
	}

	private void InitClasses()
	{
		randTop = new sRandom(randSeed.nextLong(), maxHeight - imgHeight1);
		randCount = Constants.GetMaxRandom(currentLevel);
		leftArray1 = new int[randCount];
		leftArray2 = new int[randCount];
		topArray = new int[randCount];
		leftList = new ArrayList<Integer>();
		topArray = randTop.GetArray(randCount);
//		level = Constants.GetCurrentLevelObject(level.getNum());
//		tickduration = level.getTickduration();
		GetRandomRegionValues();
		showRulesDialog();
	}

	class RepositionImgButtonTask extends TimerTask
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() 
			{  
				@Override
				public void run() 
				{
					if(!Constants.wait)
					{
						RepositionImgButton();
					}
				}
			});
		}
	}


	protected boolean RepositionImgButton()
	{
		if(getCntTimer() != null && !getCntTimer().hasStarted)
		{
			getCntTimer().start();
		}
		if(!btnSq04a.isEnabled())
		{
			btnSq04a.setEnabled(true);
			btnSq04b.setEnabled(true);
			//NEED TO UNCOMMENT FOR ACTUAL IMAGES
			//			if(isbtnSq01)
			//			{
			//				btnSq03a.setImageResource(R.drawable.crying_icon);
			//			}
			//			else
			//			{
			//				btnSq03b.setImageResource(R.drawable.glad_icon);
			//			}
		}
		if(!endGame && leftArrIndex < Constants.GetMaxRandom(currentLevel))
		{
			params = (android.widget.FrameLayout.LayoutParams) btnSq04a.getLayoutParams();
			params.leftMargin = leftArray1[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq04a.setLayoutParams(params);
			params = (android.widget.FrameLayout.LayoutParams) btnSq04b.getLayoutParams();
			params.leftMargin = leftArray2[leftArrIndex];
			params.topMargin = topArray[topArrIndex ++];
			btnSq04b.setLayoutParams(params);
			leftArrIndex ++;
		}
		return true;
	}

	@Override
	public void onClick(View view)
	{

		if(view == btnSq04a)
		{
			gameScore++;
			//NEED TO UNCOMMENT FOR ACTUAL IMAGES
			//	btnSq03a.setImageResource(R.drawable.glad_icon);
			if(gameScore == level.getSuccessLimit())
			{
				//Game finishes
				CallResultFragment();
			}
			else if(gameScore < level.getSuccessLimit())
			{

				Constants.handler.sendEmptyMessage(1);
				view.setEnabled(false);
				btnSq04b.setEnabled(false);
				isbtnSq01 = true;
				Constants.wait = false;
			}
		}
		else if(view == btnSq04b)
		{
			gameScore --;
			//NEED TO UNCOMMENT FOR ACTUAL IMAGES
			//	btnSq03a.setImageResource(R.drawable.crying_icon);
			btnSq04a.setEnabled(false);
			btnSq04b.setEnabled(false);
			isbtnSq01 = false;
		}
		SetScoreProgress();
	}

	public void StartTimer(int delay, int interval, int option)
	{
		super.StartTimer(delay, interval, new RepositionImgButtonTask(), option);
	}

	public void GetRandomRegionValues() 
	{
		leftList = GetRandomRegions(maxWidth, imgWidth1);
		for(int i =0; i < randCount; i++)
		{
			Collections.shuffle(leftList);		
			leftArray1[i] = leftList.get(0);
			leftArray2[i] = leftList.get(1);
		}
	}

}

