package com.example.squaregame;

import java.util.TimerTask;
import sph.durga.boxwithsambha.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends LevelBaseActivity implements OnClickListener {
	ImageButton btnSq;
	int imgHeight;
	int imgWidth;
	int[] leftArray;
	sRandom randLeft;
	int leftArrIndex = 0;
	TextView level012;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		gameScore = 0;
		setContentView(R.layout.level1_2);
		linLayout = (LinearLayout) findViewById(R.id.linLayout);
		sqFrame = (FrameLayout) findViewById(R.id.sqArea);
		btnSq = (ImageButton) findViewById(R.id.btnSq);
		restartBtn = (ImageButton) findViewById(R.id.restart01Btn);
		level012 = (TextView) findViewById(R.id.level1_2);
		scoreBarId = R.id.scoreBar;
		timeBarId = R.id.timeBar;
		btnSq.setOnClickListener(this);	
	}


	private void InitClasses()
	{
		randLeft = new sRandom(randSeed.nextLong(), maxWidth - imgWidth);
		randTop = new sRandom(randSeed.nextLong(), maxHeight - imgHeight);
		leftArray = new int[Constants.MAX];
		topArray = new int[Constants.MAX];
		leftArray = randLeft.GetArray(Constants.MAX);
		topArray = randTop.GetArray(Constants.MAX);

		if(currentLevel == 2)
		{
			level012.setText(R.string.round02);
		}
		showRulesDialog();
	}

	class RepositionImgButtonTask extends TimerTask
	{
		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() 
			{  
				@Override
				public void run() 
				{
					if(!Constants.wait)
					{
						RepositionImgButton();
					}
				}
			});
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus)
		{
			if(sAlertDialog == null)
			{
				maxHeight = (int) ((linLayout.getHeight())*(0.88));
				maxWidth = linLayout.getWidth();
				imgHeight = btnSq.getHeight();
				imgWidth = btnSq.getWidth();
				InitClasses();
			}
			else if(sAlertDialog.isCancelled)
			{
				StartCountDownTimer();
				StartTimer(level.getDelay1(), level.getInterval1(), 1);
			}
		}
	}

	public void StartTimer(int delay, int interval, int option)
	{

		super.StartTimer(delay, interval, new RepositionImgButtonTask(), option);
	}


	@Override
	public void onClick(View view)
	{
		if(view == btnSq)
		{
			gameScore++;
			SetScoreProgress();
			if(gameScore == level.getSuccessLimit())
			{
				//Game finishes
				CallResultFragment();
			}
			else if(gameScore < level.getSuccessLimit())
			{
				//cancel countdown timer
				//				CancelCntTimer();
				//				CancelTimer();
				//btnSq.setImageResource(R.drawable.glad_icon);
				Constants.handler.sendEmptyMessage(1);
				view.setEnabled(false);
				//	scoreTxt.setText(String.valueOf(gameScore));
				Constants.wait = false;
				//				sAnim.AnimationStart();
			}
		}
	}

	protected boolean RepositionImgButton()
	{
		if(getCntTimer() != null && !getCntTimer().hasStarted)
		{
			getCntTimer().start();
		}
		if(!btnSq.isEnabled())
		{
			btnSq.setEnabled(true);
			//btnSq.setImageResource(R.drawable.sambha);
		}
		if(!endGame && leftArrIndex < Constants.MAX)
		{
			params = (android.widget.FrameLayout.LayoutParams) btnSq.getLayoutParams();
			params.leftMargin = leftArray[leftArrIndex ++];
			params.topMargin = topArray[topArrIndex ++];
			btnSq.setLayoutParams(params);
		}
		return true;
	}


}




